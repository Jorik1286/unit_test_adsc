# This file is part of Aerospace Computing Systems.

# Aerospace Computing Systems is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later version.

# Aerospace Computing Systems is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with Aerospace
# Computing Systems. If not, see <https://www.gnu.org/licenses/>.

import os

import numpy as np

os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"


def preprocess_image(image: np.ndarray, input_shape: tuple) -> np.ndarray:
    image = image.convert("RGB")
    image = image.resize((224, 224))

    image = np.array(image) / 255.0

    image = np.array(image, dtype=np.float32)
    image = np.expand_dims(image, axis=0)

    return image


def load_model(config, path):
    if config["services"]["classifier"]["onnx"]:
        import onnxruntime as ort

        providers = ["CPUExecutionProvider"]
        model = ort.InferenceSession(path, providers=providers)

        return model
    else:
        if config["services"]["classifier"]["edge"]:
            from tflite_runtime.interpreter import Interpreter

            interpreter = Interpreter(model_path=path)
            interpreter.allocate_tensors()
        else:
            import tensorflow as tf

            interpreter = tf.lite.Interpreter(model_path=path)
            interpreter.allocate_tensors()

        return interpreter
