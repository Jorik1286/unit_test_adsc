# Рекомендации по внесению изменения в файлы markdown

Все указанные ниже расширения могут быть установлены автоматически по этой
[инструкции](../../contributing.md#2-использование-автоматических-инструментов-ide)

Используются расширения [Markdown All in
One](https://marketplace.visualstudio.com/items?itemName=yzhang.markdown-all-in-one) для
упрощения работы с файлами markdown,
[Markdown+Math](https://marketplace.visualstudio.com/items?itemName=goessner.mdmath) для
визуализации формул в формате Latex,
[markdownlint](https://marketplace.visualstudio.com/items?itemName=DavidAnson.vscode-markdownlint)
для проверки структуры файла markdown, [LTeX – LanguageTool grammar/spell
checking](https://marketplace.visualstudio.com/items?itemName=valentjn.vscode-ltex) для
проверки орфографии и пунктуации. Все расширения добавлены в рекомендации для
репозитория и могут быть установлены автоматически, см. инструкцию в секции
[2](../../contributing.md#2-использование-автоматических-инструментов-ide) файла
contributing.md.
