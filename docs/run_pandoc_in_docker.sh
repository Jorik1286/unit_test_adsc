#!/bin/bash
docker run --rm \
    --volume "$(pwd):/data" \
    --user $(id -u):$(id -g) \
    pandoc/core \
    --lua-filter=include-files.lua \
    --filter pandoc-crossref --citeproc \
    --reference-doc=custom-reference.docx \
    main.md \
    -o uemka_documentation.docx

# http://lierdakil.github.io/pandoc-crossref/

# --filter pandoc-include-code \  # https://github.com/owickstrom/pandoc-include-code

# -t docx+native_numbering \  # Дублирует Figure поле Рисунок необновляемое значение
# https://github.com/jgm/pandoc/issues/7499
# https://github.com/jgm/pandoc/issues/7451

# demo.md \
# -o demo.docx
# --toc --toc-depth=6 \
# --csl=gost-r-7-0-5-2008-numeric.csl
# --bibliography=external.bib

# To produce a custom reference.docx, first get a copy of the default reference.docx:
# pandoc -o custom-reference.docx --print-default-data-file reference.docx. Then open
# custom-reference.docx in Word, modify the styles as you wish, and save the file.
# https://pandoc.org/MANUAL.html

# docker run --rm \
#     --volume "$(pwd):/data" \
#     --user $(id -u):$(id -g) \
#     pandoc/core \
#     -o custom-reference.docx --print-default-data-file reference.docx
