# Используемые лицензии

## Численная модель КА на Python

Для сбора информации по лицензиям в используемых библиотеках использовался инструмент
[pip-licenses](https://pypi.org/project/pip-licenses/).

| Name               | Version      | License                                          |
|--------------------|--------------|--------------------------------------------------|
| importlib-metadata | 6.8.0        | Apache Software License                          |
| msgpack            | 1.0.7        | Apache Software License                          |
| requests           | 2.31.0       | Apache Software License                          |
| tenacity           | 8.2.3        | Apache Software License                          |
| tzdata             | 2023.3       | Apache Software License                          |
| xarray             | 2023.10.1    | Apache Software License                          |
| cryptography       | 41.0.5       | Apache Software License; BSD License             |
| packaging          | 23.2         | Apache Software License; BSD License             |
| python-dateutil    | 2.8.2        | Apache Software License; BSD License             |
| sniffio            | 1.3.0        | Apache Software License; MIT License             |
| astroquery         | 0.4.6        | BSD                                              |
| llvmlite           | 0.41.1       | BSD                                              |
| pyvo               | 1.4.2        | BSD                                              |
| SecretStorage      | 3.3.3        | BSD License                                      |
| astropy            | 5.3.4        | BSD License                                      |
| click              | 8.1.7        | BSD License                                      |
| contourpy          | 1.2.0        | BSD License                                      |
| cycler             | 0.12.1       | BSD License                                      |
| idna               | 3.4          | BSD License                                      |
| kiwisolver         | 1.4.5        | BSD License                                      |
| numba              | 0.58.1       | BSD License                                      |
| numpy              | 1.24.4       | BSD License                                      |
| pandas             | 2.1.3        | BSD License                                      |
| pycparser          | 2.21         | BSD License                                      |
| pyerfa             | 2.0.1.1      | BSD License                                      |
| pymap3d            | 3.0.1        | BSD License                                      |
| scipy              | 1.9.1        | BSD License                                      |
| starlette          | 0.27.0       | BSD License                                      |
| uvicorn            | 0.24.0.post1 | BSD License                                      |
| webencodings       | 0.5.1        | BSD License                                      |
| wrapt              | 1.16.0       | BSD License                                      |
| pyXSteam           | 0.4.9        | GNU General Public License v2 (GPLv2)            |
| python-can         | 4.2.2        | GNU Lesser General Public License v3 (LGPLv3)    |
| Pillow             | 10.1.0       | Historical Permission Notice                     |
|                    |              | and Disclaimer (HPND)                            |
| OrbitalPy          | 0.7.0        | MIT License                                      |
| PyYAML             | 6.0.1        | MIT License                                      |
| Represent          | 1.6.0.post0  | MIT License                                      |
| SQLAlchemy         | 2.0.23       | MIT License                                      |
| annotated-types    | 0.6.0        | MIT License                                      |
| anyio              | 3.7.1        | MIT License                                      |
| beautifulsoup4     | 4.12.2       | MIT License                                      |
| cffi               | 1.16.0       | MIT License                                      |
| charset-normalizer | 3.3.2        | MIT License                                      |
| exceptiongroup     | 1.2.0        | MIT License                                      |
| fastapi            | 0.105.0      | MIT License                                      |
| fonttools          | 4.44.0       | MIT License                                      |
| greenlet           | 3.0.2        | MIT License                                      |
| h11                | 0.14.0       | MIT License                                      |
| html5lib           | 1.1          | MIT License                                      |
| jaraco.classes     | 3.3.0        | MIT License                                      |
| jeepney            | 0.8.0        | MIT License                                      |
| jplephem           | 2.19         | MIT License                                      |
| keyring            | 24.2.0       | MIT License                                      |
| more-itertools     | 10.1.0       | MIT License                                      |
| plotly             | 5.18.0       | MIT License                                      |
| poliastro          | 0.17.0       | MIT License                                      |
| pydantic           | 2.5.2        | MIT License                                      |
| pydantic_core      | 2.14.5       | MIT License                                      |
| pyparsing          | 3.1.1        | MIT License                                      |
| pyquaternion       | 0.9.9        | MIT License                                      |
| python-can-remote  | 0.2.1        | MIT License                                      |
| pytz               | 2023.3.post1 | MIT License                                      |
| sgp4               | 2.23         | MIT License                                      |
| six                | 1.16.0       | MIT License                                      |
| soupsieve          | 2.5          | MIT License                                      |
| urllib3            | 2.0.7        | MIT License                                      |
| zipp               | 3.17.0       | MIT License                                      |
| certifi            | 2023.7.22    | Mozilla Public License 2.0 (MPL 2.0)             |
| matplotlib         | 3.8.1        | Python Software Foundation License               |
| typing_extensions  | 4.8.0        | Python Software Foundation License               |
| igrf               | 13.0.2       | UNKNOWN                                          |

На основе информации собранной в [статье](https://habr.com/ru/articles/243091/) на habr
по выбору лицензии для Open Source проекта и анализа лицензий из таблицы выше была
выбрана лицензия GPLv3. В проект добавлен файл лицензии license.txt и рекомендуемое
сообществом GNU [уведомление](https://www.gnu.org/licenses/gpl-howto.html) в верхней
части большинства файлов python.
