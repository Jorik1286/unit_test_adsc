
import React, { useState, Fragment } from 'react';
import ReactFlow, { addEdge, Background, Controls, MiniMap } from 'react-flow-renderer';

const MindNode = () => {
  const [elements, setElements] = useState([]);
  const [name, setName] = useState("");
  const [selectedElements, setSelectedElements] = useState([]);

  const addNode = () => {
    const x = window.innerWidth / 2;
    const y = window.innerHeight / 2;

    setElements(e => e.concat({
      id: (e.length + 1).toString(),
      data: { label: `${name}` },
      position: { x, y }
    }));
  };

  const deleteNode = () => {
    const nodeIdToDelete = selectedElements[0]?.id;
    const newElements = elements.filter(el => {
      if (el.id === nodeIdToDelete) {
        return false;
      }
      if (el.source === nodeIdToDelete || el.target === nodeIdToDelete) {
        return false;
      }
      return true;
    });
    setElements(newElements);
  };

  const onConnect = (params) => setElements(e => addEdge(params, e));

  const onLoad = (reactFlowInstance) => {
    reactFlowInstance.fitView();
  };

  const onSelectionChange = (elements) => {
    setSelectedElements(elements || []);
  };

  const saveJson = () => {
    const jsonContent = JSON.stringify(elements, null, 2);
    const blob = new Blob([jsonContent], { type: 'application/json' });
    const url = URL.createObjectURL(blob);
    const a = document.createElement('a');
    a.href = url;
    a.download = 'flow.json';
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
    URL.revokeObjectURL(url);
  };

  return (
    <Fragment>
      <ReactFlow
        elements={elements}
        onLoad={onLoad}
        style={{ width: '100%', height: '90vh' }}
        onConnect={onConnect}
        connectionLineStyle={{ stroke: "#ddd", strokeWidth: 2 }}
        connectionLineType="bezier"
        snapToGrid={true}
        snapGrid={[16, 16]}
        onSelectionChange={onSelectionChange}
      >
        <Background color="#888" gap={16} />
        <MiniMap
          nodeColor={n => {
            if (n.type === 'input') return 'blue';
            return '#FFCC00';
          }}
        />
        <Controls />
      </ReactFlow>

      <div>
        <input
          type="text"
          onChange={e => setName(e.target.value)}
          name="title"
        />
        <button type="button" onClick={addNode}>Add Node</button>
        <button type="button" onClick={deleteNode} disabled={selectedElements.length !== 1}>
          Delete
        </button>
        <button type="button" onClick={saveJson}>Save JSON</button>
      </div>
    </Fragment>
  );
};

export default MindNode;
