# Подключение к физической шине CAN

## Используемое оборудование

Для подключения Raspberry Pi к CAN шине используется модуль [CAN bus
MCP2515](https://duino.ru/can-bus-modul-mcp2515). Расположение контактов платы Raspberry
Pi 3 B+ представлено на @fig:rpi_pins. Схема подключения платы расширения представлена
на @fig:rpi_mcp_connection.

![Контакты платы Raspberry Pi 3 B+](images/rpi_gpio_layout.jpeg){#fig:rpi_pins}

![Подключение платы MCP 2515 к Raspberry Pi 3
B+](images/mcp2515_connection_scheme.jpeg){#fig:rpi_mcp_connection}

## Последовательность подключения

1. Убедиться, что желтый провод отключен на обеих платах.
1. Соединить одноименные провода на платах CAN bus MCP 2515 «CAN HIGH» и «CAN LOW»
   (черный и коричневый провода) между собой (черный с черным, коричневый с коричневым).
   Контроль подключения - контакт с маркировкой «Н» на первой плате подключен проводом к
   контакту с маркировкой «Н» на второй плате CAN bus MCP 2511. Аналогично с «L», см.
   @fig:mcp_board_pins.

   ![Пины high/low платы MCP
   2515](images/pins_on_can_extension_board.jpeg){#fig:mcp_board_pins}

1. Включить питание плат Raspberry Pi. Контроль: красный светодиод на платах Raspberry
   Pi – питание подано, зеленый – индикация sd-карты.
1. Подождать 30 - 40 секунд.
1. Подключить желтый провод к контакту №2 на выводах Raspberry Pi.

   ![Пин №2 raspberry](images/connect_yellow_wire_to_pin_2.png){#fig:pin_no_2_rpi}

## Программная настройка физической CAN-шины

1. Запустите bash-скрипт
  [setup_can_step_1_and_reboot.sh](setup_can_step_1_and_reboot.sh).
1. Перезагрузите raspberry.
1. Запустите bash-скрипт
  [setup_can_step_2_after_reboot.sh](setup_can_step_2_after_reboot.sh).
