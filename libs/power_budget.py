"""Set of functions to simulate power budget."""

# This file is part of Aerospace Computing Systems.

# Aerospace Computing Systems is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later version.

# Aerospace Computing Systems is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with Aerospace
# Computing Systems. If not, see <https://www.gnu.org/licenses/>.

# %%
import math

import numpy as np

from config.mission_parameters import SOLAR_PANELS_MAX_POWER


# %%
def power_income(sun_visible_part: float, e_sun_b: np.ndarray) -> float:
    """Determining the income of electricity from solar panels

    Parameters
    ----------
    sun_visible_part (float):
        Sun disk visible fraction. 0 if sun is not visible, 1 if fully visible, 0..1 if
        partially visible.
    e_sun_b (np.ndarray):
        Sun direction unit vector in body reference frame.

    Returns
    -------
    float:
        Power income
    """

    res = 0
    if sun_visible_part > 0:
        for i in range(3):
            res += (
                SOLAR_PANELS_MAX_POWER[i][round(0.5 + math.copysign(0.5, e_sun_b[i]))]
                * abs(e_sun_b[i])
                * sun_visible_part
            )
    return res
