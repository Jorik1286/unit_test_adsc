# https://www.pragmaticlinux.com/2021/10/how-to-create-a-virtual-can-interface-on-linux/
sudo modprobe vcan
sudo ip link add dev vcan0 type vcan
sudo ip link set vcan0 up
# sudo ip link set vcan0 down

# включить мониторинг посылок на шине
candump -tz vcan0
