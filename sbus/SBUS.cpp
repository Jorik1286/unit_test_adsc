/*
# This file is part of Aerospace Computing Systems.

# Aerospace Computing Systems is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later version.

# Aerospace Computing Systems is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with Aerospace
# Computing Systems. If not, see <https://www.gnu.org/licenses/>.
*/

// Bridge for CAN bus
// Bus emulator

#include <fstream>
#include <iostream>
#include <unistd.h>

#include <sys/socket.h>
#include <sys/types.h>

#include <netinet/in.h>

#include <string.h>
#include <time.h>

#include <array>
#include <cstdio>
#include <memory>
#include <stdexcept>

#include <linux/can.h>
#include <linux/can/raw.h>

#include "cans.h"
#include "cant.h"

#define THIS_NODE_BUS_ID 11
#define BROADCAST_NODE_BUS_ID 31

using namespace std;

const std::vector<std::string> StrName
    = { "Repeat",   "Flags",      "ID_CRC8", "Reciever_NET",
        "Reciever", "Sender_NET", "Sender",  "Type" };
const std::vector<uint8_t> StrLen = { 1, 5, 8, 2, 5, 2, 5, 1 };

bool SetRecID = false;
uint8_t CurRecID = THIS_NODE_BUS_ID;
uint8_t CurRecNetID = 0;

int main(int argc, char **argv)
{
    bool FINE = false; // признак выхода из цикла

    string CltVer = "version 0.0 acs 2023-04-23";

    std::string DevInName = "vcan0";
    std::string DevOutName = "vcan1";

    cout << "hello I am Bridge! ";
    cout << argc << " : ";
    for (int i = 0; i < argc; i++)
        {
            cout << argv[i] << " | ";
        };
    cout << endl;

    if (argc == 3)
        {
            DevInName = argv[1];
            DevOutName = argv[2];
        }

    cout << "DevInName=" << DevInName << " DevOutName=" << DevOutName << "/n";

    cans SocCANin;             // объект сокета
    SocCANin.start(DevInName); // старт сокета

    cans SocCANout;              // объект сокета
    SocCANout.start(DevOutName); // старт сокета

    // ОБЩИЕ ПЕРЕМЕННЫЕ КОНВЕЙЕРА

    cant FR(StrName, StrLen);

    // ГЛАВНЫЙ КОНВЕЙЕР++++++++++++++++++++++++++++++++++++++++++++++++++
    while (!FINE)
        {
            cout << " wait for Frame /n ";
            int REZ = 0;
            REZ = SocCANin.recvFrame(&FR, 1, 100);

            cout << " rlen=" << REZ << " I ";
            if (REZ < 0)
                {
                    cout << "recv ERROR!!!" << endl;
                    exit(999);
                };

            if (REZ > 0)
                {
                    cout << "After receive " << endl;
                    FR.PrintFrame();

                }; //

            int bytes_send = 0;

            bytes_send = SocCANout.sendFrame(FR);

            if (bytes_send < 0)
                {
                    cout << "ERROR CANSEND SERVER!!! " << endl;
                    sleep(1);
                };

            cout << "bytes_send =" << bytes_send << endl;

            // sleep(5);

            cout << "SEND TO SERVER OK!!! \n";

            // steps++;
            // if (steps > 1000000)
            //	steps = 0; // защита от переполнения

            // sleep(2);

        }; // while()  Главный конвейер
    // ГЛАВНЫЙ КОНВЕЙЕР++++++++++++++++++++++++++++++++++++++++++++++++++

    SocCANin.stop();
    SocCANout.stop();
    return 0;
}
